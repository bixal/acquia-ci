<?php

use Bixal\AcquiaCI\Robo\AcquiaCiCommandsTrait;
use Robo\Tasks;

/**
 * Commands used to interact with Acquia.
 *
 * This file must be in the root namespace. This is NOT put in the src/Robo/
 * Commands/ directory on purpose as that makes it unable to be extended.
 */
class AcquiaCiCommands extends Tasks
{

    use AcquiaCiCommandsTrait;

}
