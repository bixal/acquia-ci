<?php

namespace Bixal\AcquiaCI\CodeChange;

class EnvCopyCodeChange implements CodeChangeInterface
{
  protected $env;

  public function __construct($env)
  {
    $this->env = $env;
  }

  public function getEnv()
  {
    return $this->env;
  }
}
