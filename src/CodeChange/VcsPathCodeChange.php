<?php

namespace Bixal\AcquiaCI\CodeChange;

class VcsPathCodeChange implements CodeChangeInterface {
  protected $vcs_path;

  public function __construct($vcs_path)
  {
    $this->vcs_path = $vcs_path;
  }

  public function getVcsPath()
  {
    return $this->vcs_path;
  }

}
