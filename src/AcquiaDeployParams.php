<?php

namespace Bixal\AcquiaCI;

use Bixal\AcquiaCI\CodeChange\CodeChangeInterface;

class AcquiaDeployParams {

  public $aliases_to;

  public $code_change;

  public $aliases_from_db;

  public $aliases_from_files;

  public $backup_first;

  /**
   * AcquiaDeployParams constructor.
   *
   * @param string $aliases_to
   *   A comma separated list of environment aliases to update.
   * @param \Bixal\AcquiaCI\CodeChange\CodeChangeInterface $code_change
   *   If $type is vcs_path, the branch or tag, if env an alias.
   * @param string $alias_from_db
   *   The environment to copy the DB from, 0 to skip.
   * @param string $alias_from_files
   *   The environment to copy the files from, 0 to skip.
   * @param string $backup_first
   *   Create a backup of the database before starting the deployment.
   */
  public function __construct(string $aliases_to, CodeChangeInterface $code_change, string $alias_from_db = '0', string $alias_from_files = '0', string $backup_first = '0')
  {
    $this->aliases_to = $aliases_to;
    $this->code_change = $code_change;
    $this->aliases_from_db = $alias_from_db;
    $this->aliases_from_files = $alias_from_files;
    $this->backup_first = $backup_first;
  }
}
