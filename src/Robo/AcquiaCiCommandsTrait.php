<?php

namespace Bixal\AcquiaCI\Robo;

use AcquiaCloudApi\Connector\Client;
use AcquiaCloudApi\Connector\Connector;
use AcquiaCloudApi\Endpoints\Account;
use AcquiaCloudApi\Endpoints\Code;
use AcquiaCloudApi\Endpoints\Databases;
use AcquiaCloudApi\Endpoints\DatabaseBackups;
use AcquiaCloudApi\Endpoints\Environments;
use AcquiaCloudApi\Endpoints\Notifications;
use AcquiaCloudApi\Endpoints\Variables;
use AcquiaCloudApi\Exception\ApiErrorException;
use AcquiaCloudApi\Response\OperationResponse;
use Bixal\AcquiaCI\AcquiaDeployParams;
use Bixal\AcquiaCI\CodeChange\CodeChangeInterface;
use Bixal\AcquiaCI\CodeChange\EnvCopyCodeChange;
use Bixal\AcquiaCI\CodeChange\NonCodeChange;
use Bixal\AcquiaCI\CodeChange\VcsPathCodeChange;
use Consolidation\OutputFormatters\StructuredData\UnstructuredData;
use Consolidation\OutputFormatters\StructuredData\UnstructuredListData;
use Robo\Exception\TaskExitException;
use Robo\Result;
use Robo\ResultData;

/**
 * Commands used to interact with Acquia.
 *
 * This provides the actual implementation. "Use" this trait inside your own
 * Robofile if you'd like to override any of the functionality.
 *
 * This file is on purpose not in the Commands folder. If a command is
 * autoloaded in this way (even if all it does is 'use' this trait) it will not
 * be able to be overriden by your local robo file.
 */
trait AcquiaCiCommandsTrait
{

    /**
     * Running in CI environment if true.
     *
     * @var bool
     */
    protected $is_ci = FALSE;

    /**
     * The acquia subscription name.
     *
     * @var string
     */
    protected $acquiaSubName;

    /**
     * The acquia Application UUID.
     *
     * @var string
     */
    protected $acquiaApplicationUuid;

    /**
     * The drush alias to env UUID map.
     *
     * @var string
     */
    protected $acquiaEnvMap;

    /**
     * The acquia Git URL.
     *
     * @var string
     */
    protected $acquiaGitURL;

    /**
     * The acquia DB Name.
     *
     * @var string
     */
    protected $acquiaDbName;

    /**
     * The prod acquia alias.
     *
     * @var string
     */
    protected $acquiaProdEnv;

    /**
     * The acquia API User.
     *
     * @var string
     */
    protected $acquiaKey;

    /**
     * The acquia API Token.
     *
     * @var string
     */
    protected $acquiaSecret;

    /**
     * RoboFile constructor.
     */
    public function __construct()
    {
        // The commands in here expect to be run from the root directory.
        $reflection = new \ReflectionClass(\Composer\Autoload\ClassLoader::class);
        chdir(dirname(dirname(dirname($reflection->getFileName()))));
        // Include acquia-ci.yml as a config file.
        \Robo\Robo::loadConfiguration(['acquia-ci.yml']);
        $acquiaConfig = \Robo\Robo::Config()->get('acquia');
        $this->acquiaSubName = $acquiaConfig['subName'] ?? '';
        $this->acquiaApplicationUuid = $acquiaConfig['appUuid'] ?? '';
        $this->acquiaEnvMap = $acquiaConfig['envMap'] ?? [];
        $this->acquiaGitURL = $acquiaConfig['gitUrl'] ?? '';
        $this->acquiaDbName = $acquiaConfig['dbName'] ?? '';
        $this->acquiaProdEnv = $acquiaConfig['prodEnv'] ?? 'prod';
        $env = [];
        if (file_exists('.env')) {
            $env = parse_ini_file('.env', FALSE, INI_SCANNER_RAW);
        }
        // Get the environment file from environment variables first and if not
        // set, use the .env file.
        $getEnv = static function($key) use ($env) {
            return getenv($key) ?: ($env[$key] ?? '');
        };
        $this->acquiaKey = $getEnv('ACQUIA_CLOUD_API_USER');
        $this->acquiaSecret = $getEnv('ACQUIA_CLOUD_API_TOKEN');
        if (getenv('CI')) {
            $this->is_ci = TRUE;
        }
    }

    /**
     * Turn an Acquia alias into an environment UUID.
     *
     * @param $alias
     *
     * @return string
     */
    protected function acquiaAliasToUUID($alias)
    {
        if (empty($this->acquiaEnvMap)) {
            throw new \RuntimeException('The acquia.envMap is required.');
        }
        if (!empty($this->acquiaEnvMap[$alias])) {
            return $this->acquiaEnvMap[$alias];
        }
        throw new \RuntimeException(
          sprintf(
            'An Acquia alias is required: %s.',
            implode(' or ', array_keys($this->acquiaEnvMap))
          )
        );
    }

    /**
     * Retrieve the Acquia Client API.
     *
     * @return \AcquiaCloudApi\Connector\Client
     */
    protected function getAcquiaClient()
    {
        $ready = TRUE;
        if (empty($this->acquiaKey)) {
            $this->yell('ACQUIA_CLOUD_API_USER is required, it should be the Acquia key');
            $ready = FALSE;
        }
        if (empty($this->acquiaSecret)) {
            $this->yell('ACQUIA_CLOUD_API_TOKEN is required, it should be the Acquia secret');
            $ready = FALSE;
        }
        if (FALSE === $ready) {
            throw new \RuntimeException('All Acquia Environment variables are not set.');
        }
        $config = [
          'key' => $this->acquiaKey,
          'secret' => $this->acquiaSecret,
        ];

        $connector = new Connector($config);
        return Client::factory($connector);
    }

    /**
     * List all backups for $alias.
     *
     * @return \Consolidation\OutputFormatters\StructuredData\UnstructuredListData
     *
     * @command acquia:db-backups-info
     *
     * @param string $alias
     *   An Acquia alias.
     * @param string $type
     *   (Optional) Either daily or ondemand.
     */
    public function acquiaDatabaseBackupsInfo(string $alias, string $type = '')
    {
        $client = $this->getAcquiaClient();
        if ('' !== $type) {
            $client->addQuery('filter', "type=$type");
        }
        $databases_backups = new DatabaseBackups($client);
        $response = $databases_backups->getAll($this->acquiaAliasToUUID($alias), $this->acquiaDbName);

        $backups = [];
        foreach ($response as $backup) {
            /* @var \AcquiaCloudApi\Response\BackupResponse $backup */
            $backups[] = json_decode(json_encode($backup), true);
        }
        return new UnstructuredListData($backups);
    }

    /**
     * Retrieve the last ondemand DB backup for $alias.
     *
     * @return \Robo\ResultData
     *   The result with a message and id if successful.
     *
     * @command acquia:db-backups-last
     *
     * @param string $alias
     *   An Acquia alias.
     */
    public function acquiaDatabaseBackupsLast(string $alias)
    {
        $client = $this->getAcquiaClient();
        $client->addQuery('filter', "type=ondemand");
        $client->addQuery('sort', '-created');
        $client->addQuery('limit', '1');
        $databases_backups = new DatabaseBackups($client);
        $response = $databases_backups->getAll($this->acquiaAliasToUUID($alias), $this->acquiaDbName);

        if (empty($response[0])) {
            return new ResultData(ResultData::EXITCODE_ERROR, 'Unable to find any backups.', ['id' => '']);
        }
        $backup_id = $response[0]->id;
        $this->output()->write($backup_id);
        $response = new ResultData(ResultData::EXITCODE_OK, sprintf('Found backup ID %s.', $backup_id), ['id' => $backup_id]);
        return $response;
    }

    /**
     * Get backup information for $alias and backup $id.
     *
     * @return \Consolidation\OutputFormatters\StructuredData\UnstructuredListData
     *
     * @command acquia:db-backup-info
     *
     * @param string $alias
     *   An Acquia alias.
     * @param string $id
     *   The backup ID.
     */
    public function acquiaDatabaseBackupInfo(string $alias, string $id)
    {
        $client = $this->getAcquiaClient();
        $databases_backups = new DatabaseBackups($client);
        $response = $databases_backups->get($this->acquiaAliasToUUID($alias), $this->acquiaDbName, $id);
        return new UnstructuredListData(json_decode(json_encode($response), true));
    }

    /**
     * Create a database backup for $alias.
     *
     * @command acquia:db-backup-create
     *
     * @param string $alias
     *   An Acquia alias.
     * @param bool $wait_for_completion
     *   If true, the command will wait till the operation has been completed.
     */
    public function acquiaDatabaseBackupCreate(string $alias, $wait_for_completion = true)
    {
        $client = $this->getAcquiaClient();
        $operation_description = sprintf('Database backup of %s', $alias);
        $databases_backups = new DatabaseBackups($client);
        $operation_response = $databases_backups->create($this->acquiaAliasToUUID($alias), $this->acquiaDbName);
        return $this->acquiaOperationResult($wait_for_completion, $operation_response, $operation_description);
    }

    /**
     * Restore database backup $id for $alias.
     *
     * @command acquia:db-backup-restore
     *
     * @param string $alias
     *   An Acquia alias.
     * @param string $id
     *   The backup ID.
     * @param bool $wait_for_completion
     *   If true, the command will wait till the operation has been completed.
     */
    public function acquiaDatabaseBackupRestore(string $alias, string $id, $wait_for_completion = true)
    {
        $client = $this->getAcquiaClient();
        $operation_description = sprintf('Database restoration of %s with backup %s', $alias, $id);
        $databases_backups = new DatabaseBackups($client);
        $operation_response = $databases_backups->restore($this->acquiaAliasToUUID($alias), $this->acquiaDbName, $id);
        return $this->acquiaOperationResult($wait_for_completion, $operation_response, $operation_description);
    }

    /**
     * Show information about an environment.
     *
     * @return \Consolidation\OutputFormatters\StructuredData\UnstructuredData
     *
     * @command acquia:environment
     *
     * @param string $alias
     *   An Acquia alias.
     */
    public function acquiaEnvironmentInfo(string $alias)
    {
        $client = $this->getAcquiaClient();
        $environments = new Environments($client);
        $response = $environments
          ->get($this->acquiaAliasToUUID($alias));
        return new UnstructuredData(json_decode(json_encode($response), true));
    }

    /**
     * Show information about an environment.
     *
     * @command acquia:vcs-path
     *
     * @param string $alias
     *   An Acquia alias.
     *
     * @return \Robo\ResultData
     *   The result data in 'vcs_path'.
     */
    public function acquiaVcsPath(string $alias)
    {
        $client = $this->getAcquiaClient();
        $environments = new Environments($client);
        $response = $environments
          ->get($this->acquiaAliasToUUID($alias));
        $vcs_path = $response->vcs->path;
        $this->output()->write($vcs_path);
        return new ResultData(ResultData::EXITCODE_OK, sprintf('%s is on VCS path %s.', $alias, $vcs_path), ['vcs_path' => $vcs_path]);
    }

    /**
     * List out tasks.
     *
     * @command drush:status
     *
     * @param string $alias
     *   An Acquia alias.
     *
     * @return \Robo\Result
     */
    public function drushStatus(string $alias)
    {
        return $this->drush([$alias, 'status']);
    }

    /**
     * Given an alias like dev convert to @wioaspp.dev.
     *
     * @param string $alias
     *   A Drush shorthand alias, like dev.
     *
     * @return string
     *   A full Drush alias, like @wioaspp.dev.
     */
    protected function drushAliasToFullAlias(string $alias)
    {
        // If @ is already in the alias, remove it, then add it back.
        $alias = str_replace('@', '', $alias);
        // If no drush site is given, use the current acquia subscription name.
        if (FALSE === stristr($alias, '.')) {
            $alias = "{$this->acquiaSubName}.$alias";
        }
        return "@$alias";
    }

    /**
     * Run a drush command with arguments.
     *
     * @command drush
     *
     * @param array $args
     *   Pass-through arguments for drush.
     *
     *  @return \Robo\Result
     */
    public function drush(array $args)
    {
        $args[0] = $this->drushAliasToFullAlias($args[0]);
        return $this->taskExec('./drush.sh')->args($args)->run();
    }


    /**
     * Import configuration on $alias environment.
     *
     * @command drush:config-import
     *
     * @param string $alias
     *   An Acquia alias.
     */
    public function drushConfigImport(string $alias)
    {
        // Rebuilding cache is important if there are new modules being
        // installed so that Drupal doesn't think those modules don't exist
        // when their config is being imported.
        $this->drushCacheRebuild($alias);
        // Import all config split definitions that are symlinked to their own directory.
        // This is required in rare circumstances config to enable a module is in a config
        // split but that config split has not been installed yet. This can cause it to
        // think the module should be disabled but it tries to import its config.
        $this->drush([$alias, 'config:import', '--partial', '--source=' . $this->acquiaGetWebRoot($alias)]);
        // Import the config twice so that new config split settings will import.
        $this->drush([$alias, 'config:import', '-y']);
        $this->drush([$alias, 'config:import', '-y']);
    }

    /**
     * Run outstanding update hooks on $alias environment.
     *
     * @command drush:updb
     *
     * @param string $alias
     *   An Acquia alias.
     */
    public function drushUpdateDatabase(string $alias)
    {
        $this->drush([$alias, 'updb', '-y']);
        $this->drushCacheRebuild($alias);
    }

    /**
     * Run drush deploy on $alias environment.
     *
     * @command drush:deploy
     *
     * @param string $alias
     *   An Acquia alias.
     */
    public function drushDeploy(string $alias)
    {
        $this->drush([$alias, 'deploy']);
    }

    /**
     * Clear cache.
     *
     * @command drush:cache-rebuild
     *
     * @param string $alias
     *   An Acquia alias.
     *
     * @return \Robo\Result
     */
    public function drushCacheRebuild(string $alias)
    {
        return $this->drush([$alias, 'cache-rebuild']);
    }

    /**
     * Turn maintenance mode off or on on an environment.
     *
     * @command drush:maintenance-mode
     *
     * @param string $alias
     *   An Acquia alias.
     * @param bool $enabled
     *   True for maintenance mode or false to disable.
     *
     * @return \Robo\Result
     */
    public function drushMaintenanceMode(string $alias, bool $enabled = true)
    {
        return $this->drush([$alias, 'state:set', 'system.maintenance_mode', true === $enabled ? 1 : 0, '--input-format=integer']);
    }

    /**
     * Run a composer command with arguments.
     *
     * @command composer
     *
     * @param array $args
     *   Pass-through arguments for composer.
     *
     * @return \Robo\Result
     */
    public function composer(array $args)
    {
        return $this->taskExec('./composer.sh')->args($args)->run();
    }

    /**
     * Create a variable or update it to $value if needed.
     *
     * @command acquia:variable-upsert
     *
     * @param string $aliases_to
     *   A comma separated list of environment aliases to update.
     * @param string $name
     *    It must contain only letters, numbers, and underscores; it cannot
     *    start with a number, AH_, or ACQUIA_; and it must be under 256
     *    characters long.
     * @param string $value
     *   A variable value. Cannot have spaces in it, will cause a 500 error with
     *   no an 'Unkown error' if you do so.
     * @param bool $hide_value
     *   Don't print out to the log the $value.
     * @param bool $wait_for_completion
     *   If true, the command will wait till the operation has been completed.
     *
     * @throws \Robo\Exception\TaskExitException
     */
    public function acquiaVariableUpsert(string $aliases_to, string $name, string $value = '', bool $hide_value = true, bool $wait_for_completion = true)
    {
      $value_log = $value;
      if (true === $hide_value) {
        $value_log = '<hidden>';
      }
      $this->applyToAllAliases($aliases_to, function($alias_to) use ($name, $value, $value_log, $wait_for_completion) {
        $operation_response = NULL;
        $operation_description = '';
        $client = $this->getAcquiaClient();
        $variables = new Variables($client);
        $context = sprintf('variable "%s" with value "%s" on %s', $name, $value_log, $alias_to);
        try {
          $variable_response = $variables->get(
            $this->acquiaAliasToUUID($alias_to),
            $name
          );
          if ($variable_response->value === $value) {
            $message = sprintf('The %s is already set with that value.', $context);
            $this->say($message);
          }
          else {
            $operation_description = sprintf('Updating %s', $context);
            $operation_response = $variables->update(
              $this->acquiaAliasToUUID($alias_to),
              $name,
              $value
            );
          }
        } catch (ApiErrorException $exception) {
          if ($exception->getResponseBody()->message === 'The environment variable you are trying to access does not exist, or you do not have permission to access it.') {
            $operation_description = sprintf('Creating %s', $context);
            $operation_response = $variables->create(
              $this->acquiaAliasToUUID($alias_to),
              $name,
              $value
            );
          }
        }
        // Run the update or create command if required.
        if ($operation_response instanceof OperationResponse) {
          $this->acquiaOperationResult(
            $wait_for_completion,
            $operation_response,
            $operation_description
          );
        }
      });
    }

    /**
     * Build out files for compatibility with Acquia file system.
     *
     * @command acquia:build-and-push-code
     *
     * @param string $branch
     *   The branch to push to.
     * @param string $build_number
     *   The unique ID used to create the tag (tags/$branch-$build_number).
     *
     * @return string
     *   The tag created.
     *
     * @throws \Robo\Exception\TaskExitException
     */
    public function acquiaBuildAndPushCode(string $branch, string $build_number)
    {
        // @TODO: Perhaps abstract these so others can use.
        if (!file_exists('./scripts/acquia-build-code.sh')) {
            throw new TaskExitException(static::class, './scripts/acquia-build-code.sh does not exist, you cannot build code.', Result::EXITCODE_ERROR);
        }
        if (!file_exists('./scripts/acquia-push-code.sh')) {
            throw new TaskExitException(static::class, './scripts/acquia-push-code.sh does not exist, you cannot push code.', Result::EXITCODE_ERROR);
        }
        $this->say('Building code for Acquia now...');
        $this->taskExec('./scripts/acquia-build-code.sh')->run();
        $this->say('Finished building code for Acquia');
        $this->say('Pushing code to Acquia now...');
        $this->taskExec('./scripts/acquia-push-code.sh')->args([$this->acquiaGitURL, $branch, $build_number])->run();
        $this->say("Pushed code to branch $branch");
        $tag = $this->acquiaCreateVcsPath($branch, $build_number);
        $this->say("Pushed code to tag $tag");
        $this->say('Finished pushing code to Acquia');
        return $tag;
    }

    /**
     * Download the Acquia aliases to the current directory.
     *
     * @command acquia:aliases
     *
     * @param int $version
     *   The version of Drush aliases to download.
     */
    public function acquiaAliases(int $version = 9)
    {
        $client = $this->getAcquiaClient();
        $client->addQuery('version', $version);
        $this->say('Downloading Acquia Drush Aliases');
        $account = new Account($client);
        $result = $account->getDrushAliases();
        $file_name = sprintf('acquia-cloud.drush-%d-aliases.tar.gz', $version);
        $this->taskWriteToFile($file_name)->text((string) $result)->run();
        $this->say(sprintf('Your Acquia aliases have been downloaded to %s', realpath($file_name)));
    }

    /**
     * Switch code on $alias to tag $tag.
     *
     * @command acquia:switch-code
     *
     * @param string $alias
     *   An Acquia alias.
     * @param string $vcs_path
     *   A tag name (ie, tags/release-1234) or branch (ie, release).
     * @param bool $wait_for_completion
     *   If true, the command will wait till the operation has been completed.
     *
     * @return \Robo\Result|\Robo\ResultData
     *   A result.
     */
    public function acquiaSwitchCode(string $alias, string $vcs_path, $wait_for_completion = true)
    {
        $client = $this->getAcquiaClient();
        $operation_description = sprintf('Code switch started (%s env to %s tag / branch)', $alias, $vcs_path);
        $code = new Code($client);
        $operation_response = $code->switch($this->acquiaAliasToUUID($alias), $vcs_path);
        return $this->acquiaOperationResult($wait_for_completion, $operation_response, $operation_description);
    }

    /**
     * Copy code to $alias_to from $alias_from.
     *
     * @command acquia:copy-code
     *
     * @param string $alias_to
     *   Copy code to Acquia alias.
     * @param string|null $alias_from
     *   Copy code from Acquia alias (Default prod).
     * @param bool $wait_for_completion
     *   If true, the command will wait till the operation has been completed.
     *
     * @return \Robo\Result|\Robo\ResultData
     *   A result.
     */
    public function acquiaCopyCode(string $alias_to, string $alias_from = null, $wait_for_completion = true)
    {
        $client = $this->getAcquiaClient();
        $alias_from = $alias_from ?: $this->acquiaProdEnv;
        $operation_description = sprintf('Code copy (%s -> %s)', $alias_from, $alias_to);
        $code = new Code($client);
        $operation_response = $code->deploy($this->acquiaAliasToUUID($alias_from), $this->acquiaAliasToUUID($alias_to));
        return $this->acquiaOperationResult($wait_for_completion, $operation_response, $operation_description);
    }

    /**
     * Copy files to $alias_to from $alias_from.
     *
     * @command acquia:copy-files
     *
     * @param string $alias_to
     *   Copy files to Acquia alias.
     * @param string|null $alias_from
     *   Copy files from Acquia alias (Default prod).
     * @param bool $wait_for_completion
     *   If true, the command will wait till the operation has been completed.
     *
     * @return \Robo\Result|\Robo\ResultData
     *   A result.
     */
    public function acquiaCopyFiles(string $alias_to, string $alias_from = null, $wait_for_completion = true)
    {
        $client = $this->getAcquiaClient();
        $alias_from = $alias_from ?: $this->acquiaProdEnv;
        if ($this->acquiaProdEnv === $alias_to) {
            throw new TaskExitException(static::class, 'You may not copy files to production', Result::EXITCODE_ERROR);
        }
        $operation_description = sprintf('File copy (%s -> %s)', $alias_from, $alias_to);
        $environments = new Environments($client);
        $operation_response = $environments->copyFiles($this->acquiaAliasToUUID($alias_from), $this->acquiaAliasToUUID($alias_to));
        return $this->acquiaOperationResult($wait_for_completion, $operation_response, $operation_description);
    }

    /**
     * Copy the database to $alias_to from $alias_from.
     *
     * @command acquia:copy-db
     *
     * @param string $alias_to
     *   Copy DB to Acquia alias.
     * @param string|null $alias_from
     *   Copy DB from Acquia alias (Default prod).
     * @param bool $wait_for_completion
     *   If true, the command will wait till the operation has been completed.
     *
     * @return \Robo\Result|\Robo\ResultData
     *   A result.
     */
    public function acquiaCopyDatabase(string $alias_to, string $alias_from = null, $wait_for_completion = true)
    {
        $client = $this->getAcquiaClient();
        $alias_from = $alias_from ?: $this->acquiaProdEnv;
        if ($this->acquiaProdEnv === $alias_to) {
            throw new TaskExitException(static::class, 'You may not copy DB to production', Result::EXITCODE_ERROR);
        }
        $operation_description = sprintf('DB Copy (%s -> %s)', $alias_from, $alias_to);
        $databases = new Databases($client);
        $operation_response = $databases->copy($this->acquiaAliasToUUID($alias_from), $this->acquiaDbName, $this->acquiaAliasToUUID($alias_to));
        return $this->acquiaOperationResult($wait_for_completion, $operation_response, $operation_description);
    }

    /**
     * Retrieve the status of a notification for output.
     *
     * @command acquia:notification
     *
     * @param string $notification_uuid
     *   A notification UUID.
     *
     * @return \Consolidation\OutputFormatters\StructuredData\UnstructuredListData
     */
    public function acquiaNotification(string $notification_uuid)
    {
        $response = $this->acquiaNotificationResponse($notification_uuid);
        return new UnstructuredListData(json_decode(json_encode($response), true));
    }

    /**
     * Retrieve the status of a notification as a response.
     *
     * @command acquia:notification
     *
     * @param string $notification_uuid
     *   A notification UUID.
     *
     * @return \AcquiaCloudApi\Response\NotificationResponse
     */
    protected function acquiaNotificationResponse(string $notification_uuid)
    {
        $client = $this->getAcquiaClient();
        $notifications = new Notifications($client);
        return $notifications->get($notification_uuid);
    }

    /**
     * Return the results of an endpoint that returns an operations response.
     *
     * This commands finish fast and continue in the background. They return
     * a link to a notification where you can check its progress.
     *
     * @param bool $wait_for_completion
     *   Show the program stop until the notification task has finished?
     * @param \AcquiaCloudApi\Response\OperationResponse $operation_response
     *   The response from the API.
     * @param string $operation_description
     *   A description of what the endpoint does.
     * @param int|float $timeout
     *   How long to wait until we abort checking for the notification completion.
     *
     * @return \Robo\Result|\Robo\ResultData
     *   A result.
     *
     * @throws \Robo\Exception\TaskExitException
     */
    protected function acquiaOperationResult(bool $wait_for_completion, OperationResponse $operation_response, string $operation_description, int $timeout = 60 * 20)
    {
        if (true === $wait_for_completion) {
            $this->say($operation_description . ' has started.');
            return $this->acquiaWaitForOperationToComplete($operation_response, $operation_description, $timeout);
        }
        $not_finished_message = $operation_description . ' has started in the background and may or may not be finished yet.';
        $this->say($not_finished_message);
        return Result::message($not_finished_message);
    }

    /**
     * @param \AcquiaCloudApi\Response\OperationResponse $operation_response
     *   The response from the API.
     * @param string $operation_description
     *   A description of what the endpoint does.
     * @param int $timeout
     *   How long to wait until we abort checking for the notification completion.
     *
     * @return \Robo\Result|\Robo\ResultData
     *   A result.
     *
     * @throws \Robo\Exception\TaskExitException
     */
    protected function acquiaWaitForOperationToComplete(OperationResponse $operation_response, string $operation_description, int $timeout)
    {
        $client = $this->getAcquiaClient();
        $notifications = new Notifications($client);
        $parts = explode('/', $operation_response->links->notification->href);
        $notification_uuid = end($parts);
        if (empty($notification_uuid)) {
            throw new TaskExitException(
              static::class,
              sprintf('Unable to %s, a notification UUID could not be found in the response: %s', $operation_description, print_r($operation_response, 1)),
              Result::EXITCODE_ERROR);
        }

        $time_start = time();
        $timeout_at = $time_start + $timeout;
        // The progress property is never updated from 0.
        // $progress_bar = new ProgressBar($this->output, 100);
        // $progress_bar->start();
        do {
            sleep(10);
            $response = $notifications->get($notification_uuid);
            switch ($response->status) {
                case 'completed':
                    // $progress_bar->finish();
                    $this->say(' ');
                    $this->say($operation_description . ' is now complete.');
                    return Result::message($operation_description . ' is now complete.');

                case 'failed':
                    // $progress_bar->finish();
                    $this->say(' ');
                    throw new TaskExitException(static::class, $operation_description . ' has failed.', Result::EXITCODE_ERROR);

                case 'in-progress':
                    // $progress_bar->setProgress($response->progress);
                    // Write a dot for each loop.
                    $this->output()->write('.');
                    break;

                default:
                    // $progress_bar->finish();
                    $this->say(' ');
                    throw new TaskExitException(static::class, sprintf(' %s returned an unsupported status, %s.', $operation_response, $response->status), Result::EXITCODE_ERROR);

            }
        } while (time() < $timeout_at);
        // $progress_bar->finish();
        $this->say(' ');
        throw new TaskExitException(static::class, sprintf('%s never completed after %d seconds.', $operation_description, time() - $time_start), Result::EXITCODE_ERROR);
    }

    /**
     * Get the web root on an Acquia environment.
     *
     * @param string $alias
     *   An Acquia alias.
     *
     * @return string
     */
    protected function acquiaGetWebRoot(string $alias)
    {
        return "/var/www/html/{$this->acquiaSubName}.$alias/config/config_splits";
    }

    /**
     * Build a new tag and deploy it on $alias.
     *
     * @command acquia:deploy-new-build
     *
     * @param string $aliases_to
     *   A comma separated list of environment aliases to update.
     * @param string $branch
     *   The branch to push to.
     * @param string $build_number
     *   The unique ID used to create the tag (tags/$branch-$build_number).
     * @param string $backup_first
     *   Create a backup of the database before starting the deployment.
     */
    public function acquiaDeployNewBuild(string $aliases_to, string $branch, string $build_number, string $backup_first = '0')
    {
        $client = $this->getAcquiaClient();
        $vcs_path = $this->acquiaBuildAndPushCode($branch, $build_number);
        $this->acquiaDeployVcsPath($aliases_to, $vcs_path, '0', '0', $backup_first);
    }

    /**
     * Deploy a copy of the code, db, and/or files to 1 or more diff envs.
     *
     * @param string $aliases_to
     *   A comma separated list of environment aliases to update.
     * @param \Bixal\AcquiaCI\CodeChange\CodeChangeInterface $code_change
     *   If $type is vcs_path, the branch or tag, if env an alias.
     * @param string $alias_from_db
     *   The environment to copy the DB from, 0 to skip.
     * @param string $alias_from_files
     *   The environment to copy the files from, 0 to skip.
     * @param string $backup_first
     *   Create a backup of the database before starting the deployment.
     */
    protected function acquiaDeploy(string $aliases_to, CodeChangeInterface $code_change, string $alias_from_db = '0', string $alias_from_files = '0', string $backup_first = '0')
    {
        $args = new AcquiaDeployParams(...func_get_args());
        $this->applyToAllAliases($aliases_to, function($alias_to) use ($code_change, $alias_from_db, $alias_from_files, $backup_first, $args) {
            if ('1' === $backup_first) {
                $this->acquiaDatabaseBackupCreate($alias_to);
                $result = $this->acquiaDatabaseBackupsLast($alias_to);
                if ($result->getExitCode() === Result::EXITCODE_OK) {
                    $this->say(sprintf('A database backup with ID %s has been created on environment %s. Restore using the command acquia:db-backup-restore %s %s', $result->getArrayCopy()['id'], $alias_to, $alias_to, $result->getArrayCopy()['id']));
                }
                else {
                    throw new TaskExitException(
                      static::class,
                      'Unable to create a database backup for deployment, exiting.',
                      Result::EXITCODE_ERROR
                    );
                }
            }
            // If the Drush command to put the site into maintenance mode, don't
            // even attempt to continue with the deployment.
            $this->drushMaintenanceMode($alias_to, TRUE)->stopOnFail();
            // Get the current VCS path of the environment before it is switched.
            $result = $this->acquiaVcsPath($alias_to);
            if ($result->getExitCode() === Result::EXITCODE_OK) {
                $current_vcs_path = $result['vcs_path'];
            }
            else {
                throw new TaskExitException(
                  static::class,
                  'Unable to determine current code before switching.',
                  Result::EXITCODE_ERROR
                );
            }
            // Copy code from another environment.
            if ($code_change instanceof EnvCopyCodeChange) {
                $code = $code_change->getEnv();
                if ($alias_to !== $code) {
                    $this->say(sprintf('%s is currently on %s before switching to %s\'s code.', $alias_to, $current_vcs_path, $code));
                    $this->hookPreCodeChange($args);
                    $this->acquiaCopyCode($alias_to, $code);
                }
                else {
                  throw new TaskExitException(
                    static::class,
                    'Unable to copy code from the same environment.',
                    Result::EXITCODE_ERROR
                  );
                }
            }
            // Change code to a branch or tag.
            elseif ($code_change instanceof VcsPathCodeChange) {
                $code = $code_change->getVcsPath();
                $this->say(sprintf('%s is currently on %s before switching to %s.', $alias_to, $current_vcs_path, $code));
                $this->hookPreCodeChange($args);
                $this->acquiaSwitchCode($alias_to, $code);
            }
            elseif ($code_change instanceof NonCodeChange) {
                $this->say('No code change was specified.');
            }
            else {
                throw new TaskExitException(
                  static::class,
                  '$type must be either env or vcs_path.',
                  Result::EXITCODE_ERROR
                );
            }
            $copied_db = '0' !== $alias_from_db && $alias_to !== $alias_from_db;
            if (TRUE === $copied_db) {
                $this->acquiaCopyDatabase($alias_to, $alias_from_db);
            }
            $copied_files = '0' !== $alias_from_files && $alias_to !== $alias_from_files;
            if (TRUE === $copied_files) {
                $this->acquiaCopyFiles($alias_to, $alias_from_files);
            }
            $this->drushDeploy($alias_to);
            // Once the site is up to date and ready and a DB has been copied
            // to it, allow running extra commands.
            if (TRUE === $copied_db) {
              $this->hookDbCopiedSiteReady($args);
            }
            $this->drushMaintenanceMode($alias_to, FALSE);
        });
    }

    /**
     * Run commands after the site is prepared and a DB copy has happened.
     *
     * @param \Bixal\AcquiaCI\AcquiaDeployParams $params
     *   The parameters passed to acquiaDeploy.
     */
    protected function hookDbCopiedSiteReady(AcquiaDeployParams $params) {
        $this->say('If you would like to run commands after the site is ready and a DB has been copied, override hookDbCopiedSiteReady.');
    }

    /**
     * Run commands after the site is down but the code has not been changed.
     *
     * @param \Bixal\AcquiaCI\AcquiaDeployParams $params
     *   The parameters passed to acquiaDeploy.
     */
    protected function hookPreCodeChange(AcquiaDeployParams $params) {
      $this->say('If you would like to run commands after the site is down but before code is change, override hookPreCodeChange.');
    }

    /**
     * Deploy a copy of the code, db, and/or files to 1 or more diff envs.
     *
     * @command acquia:deploy-branch-or-tag
     *
     * @param string $aliases_to
     *   A comma separated list of environment aliases to update.
     * @param string $vcs_path
     *   A tag name (ie, tags/release-1234) or branch (ie, release).
     * @param string $alias_from_db
     *   The environment to copy the DB from, 0 to skip.
     * @param string $alias_from_files
     *   The environment to copy the files from, 0 to skip.
     * @param string $backup_first
     *   Create a backup of the database before starting the deployment.
     */
    public function acquiaDeployVcsPath(string $aliases_to, string $vcs_path, string $alias_from_db = '0', string $alias_from_files = '0', string $backup_first = '0')
    {
        $code_change = new VcsPathCodeChange($vcs_path);
        $args = func_get_args();
        $args[1] = $code_change;
        $this->acquiaDeploy(...$args);
    }

    /**
     * Deploy a copy of the code, db, and/or files to 1 or more diff envs.
     *
     * @command acquia:deploy-copy
     *
     * @param string $aliases_to
     *   A comma separated list of environment aliases to update.
     * @param string $alias_from_code
     *   The environment to copy the code from.
     * @param string $alias_from_db
     *   The environment to copy the DB from, 0 to skip.
     * @param string $alias_from_files
     *   The environment to copy the files from, 0 to skip.
     * @param string $backup_first
     *   Create a backup of the database before starting the deployment.
     */
    public function acquiaDeployCopy(string $aliases_to, string $alias_from_code, string $alias_from_db = '0', string $alias_from_files = '0', string $backup_first = '0')
    {
        $code_change = new EnvCopyCodeChange($alias_from_code);
        $args = func_get_args();
        $args[1] = $code_change;
        $this->acquiaDeploy(...$args);
    }

    /**
     * Copy the prod code, DB, and files to $aliases_to.
     *
     * @command acquia:deploy-clone-from-prod
     *
     * @param string $aliases_to
     *   A comma separated list of environment aliases to update.
     * @param string $backup_first
     *   Create a backup of the database before starting the deployment.
     *
     * @throws \Robo\Exception\TaskExitException
     */
    public function acquiaDeployCloneFromProd(string $aliases_to, string $backup_first = '0')
    {
        $code_change = new EnvCopyCodeChange($this->acquiaProdEnv);
        $this->acquiaDeploy($aliases_to, $code_change, $this->acquiaProdEnv, $this->acquiaProdEnv, $backup_first);
    }

    /**
     * Copy the prod DB and files to $aliases_to.
     *
     * @command acquia:deploy-refresh-from-prod
     *
     * @param string $aliases_to
     *   A comma separated list of environment aliases to update.
     * @param string $backup_first
     *   Create a backup of the database before starting the deployment.
     *
     * @throws \Robo\Exception\TaskExitException
     */
    public function acquiaDeployRefreshFromProd(string $aliases_to, string $backup_first = '0')
    {
      $this->acquiaDeployNonCodeChange($aliases_to, $this->acquiaProdEnv, $this->acquiaProdEnv, $backup_first);
    }

    /**
     * Optionally copy DB and files to $alias_to.
     *
     * @command acquia:deploy-non-code-change
     *
     * @param string $aliases_to
     *   A comma separated list of environment aliases to update.
     * @param string $backup_first
     *   Create a backup of the database before starting the deployment.
     *
     * @throws \Robo\Exception\TaskExitException
     */
    public function acquiaDeployNonCodeChange(string $aliases_to, string $alias_from_db = '0', string $alias_from_files = '0', string $backup_first = '0')
    {
      $code_change = new NonCodeChange();
      $this->acquiaDeploy($aliases_to, $code_change, $alias_from_db, $alias_from_files, $backup_first);
    }

    /**
     * Run the $function on each alias in $aliases_to.
     *
     * @param string $aliases_to
     *   A comma separated list of environment aliases to update.
     * @param callable $function
     *   A callback to apply to each alias.
     */
    protected function applyToAllAliases(string $aliases_to, callable $function)
    {
        $aliases_to = array_filter(explode(',', $aliases_to));
        foreach ($aliases_to as $alias_to) {
            $function($alias_to);
        }
    }

    /**
     * Create a VCS Path from a branch + build.
     *
     * @param string $branch
     *   The branch this tag was was built from.
     * @param string $build_number
     *   A unique ID.
     *
     * @return string
     *   A full tag name (tags/*).
     */
    protected function acquiaCreateVcsPath(string $branch, string $build_number) {
        return sprintf('tags/%s-%s', $branch, $build_number);
    }

    /**
     * Install a fresh copy of the site with the current config.
     *
     * @command drush:fresh-install
     *
     * @param string $alias
     *   An Acquia alias.
     *
     * @throws \Robo\Exception\TaskExitException
     */
    public function drushFreshInstall(string $alias)
    {
        if ($this->acquiaProdEnv === $alias) {
            throw new TaskExitException(
                static::class,
                'You may not do a fresh install on production',
                Result::EXITCODE_ERROR
            );
        }
        $this->drush([$alias, 'sql:drop', '-y'])->stopOnFail();
        $this->yell(
            'This cache rebuild command will just run WILL throw errors, please ignore. It is required for site install to work.'
        );
        $this->drushCacheRebuild($alias);
        $this->drush([$alias, 'si', '-y', '--account-pass=admin', '--existing-config', 'minimal'])->stopOnFail(
        );
        $this->drushCacheRebuild($alias);
        // Don't use $this->drushConfigImport() because that imports the config
        // splits by itself which does not work if that module is not enabled
        // yet.
        $this->drush([$alias, 'config:import', '-y']);
        $this->drush([$alias, 'config:import', '-y']);
    }

}
